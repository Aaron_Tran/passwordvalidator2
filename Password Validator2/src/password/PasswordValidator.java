package password;

/*
 * @author Aaron Tran 
 * Student Number 991525227
 * This class is used to validate passwords length and number of digits
 * assumption is that spaces are not valid character
 */

public class PasswordValidator {
	
	public static boolean hasValidCaseChars(String password) {
		return password != null && password.matches(".*[A-Z]+.*") && password.matches(".*[a-z]+.*");
	}
	
	//variable for min length of string
	private static int MIN_LENGTH = 8;
	/*
	 * validates length of password, blank spaces are not considered valid characters
	 * @param password
	 * @return boolean
	 */
	public static boolean isValidLength(String password) {
		return password != null && password.trim().length() >= MIN_LENGTH;
	}
	
	//variable for min number of digits
	private static int MIN_DIGITS = 2;
	/*
	 * validates number of digits in password is min requirement
	 * @param password
	 * @return boolean
	 */	
	public static boolean hasEnoughDigits( String password ) {
		int digits = 0;
		//check if password is null
		if(password == null) {
			return false;
		}
		//iterate string to count digits
		for (int i=0; i < password.trim().length(); i++) {
			if (Character.isDigit(password.trim().charAt(i))) {
				digits++;
			}
		}
		//checks if valid number of digits
		return digits >= MIN_DIGITS;
	}
}
