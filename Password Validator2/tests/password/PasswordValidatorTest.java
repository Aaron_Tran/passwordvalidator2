package password;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * @author Aaron Tran 
 * Student Number 991525227
 * This class is used to test PasswordValidator methods
 */

public class PasswordValidatorTest {
	
	@Test
	public void testHasValidCaseChars() {
		boolean result = PasswordValidator.hasValidCaseChars("aAaaAa");
		assertTrue("invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryIn() {
		boolean result = PasswordValidator.hasValidCaseChars("aA");
		assertTrue("invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionBlank() {
		boolean result = PasswordValidator.hasValidCaseChars("");
		assertFalse("invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionNull() {
		boolean result = PasswordValidator.hasValidCaseChars(null);
		assertFalse("invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionNumbers() {
		boolean result = PasswordValidator.hasValidCaseChars("123");
		assertFalse("invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutUpper() {
		boolean result = PasswordValidator.hasValidCaseChars("AAA");
		assertFalse("invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutLower() {
		boolean result = PasswordValidator.hasValidCaseChars("aaa");
		assertFalse("invalid case characters", result);
	}
	

	/*
	 * Tests validates that passwords length greater than or equal to 8 pass
	 */
	@Test
	public void testIsValidLengthRegular() {
		assertTrue("Invalid password length", PasswordValidator.isValidLength("1234567890"));
		
	}
	
	//Test validates passwords shorter than 8 characters is invalid
	@Test
	public void testIsValidLengthException() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength(""));
	}
	
	//Test validates passwords shorter than 8 characters is invalid not including spaces at end
	@Test
	public void testIsValidLengthExceptionSpaces() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength("                      "));
	}
	
	//null password should return false
	@Test
	public void testIsValidLengthExceptionNull() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength(null));
	}
	
	//the exact password length of 8 should return true
	@Test
	public void testIsValidLengthBoundaryIn() {
		assertTrue("Invalid password length", PasswordValidator.isValidLength("12345678"));
	}
	
	//when password length is exactly one char less than min requirement returns false
	@Test
	public void testIsValidLengthBoundaryOut() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength("1234567"));
	}
	
	//passwords with at least 2 digits is a valid
	@Test
	public void testHasEnoughDigitsRegular() {
		assertTrue("Invalid number of digits in password", PasswordValidator.hasEnoughDigits("1234567"));
	}
	
	//password with no digits is invalid
	@Test
	public void testHasEnoughDigitsException() {
		assertFalse("Invalid number of digits in password", PasswordValidator.hasEnoughDigits(""));
	}

	//null password is invalid
	@Test
	public void testHasEnoughDigitsExceptionNull() {
		assertFalse("Invalid number of digits in password", PasswordValidator.hasEnoughDigits(null));
	}
	
	//passwords with exactly 2 digits are valid
	@Test
	public void testHasEnoughDigitsBoundaryIn() {
		assertTrue("Invalid number of digits in password", PasswordValidator.hasEnoughDigits("12"));
	}

	//passwords with exactly 1 less than min are invalid i.e. 1 digit only
	@Test
	public void testHasEnoughDigitsBoundaryOut() {
		assertFalse("Invalid number of digits in password", PasswordValidator.hasEnoughDigits("1"));
	}
}









